//var sqlite3 = require('sqlite3');

var Promise = require('bluebird');
var sqlite3 = Promise.promisifyAll(require('sqlite3'));
var db = new sqlite3.Database('./db/db.db');
var _ = require('lodash');
var array = require('lodash/array');


var init = function() {
	return new Promise(function(done,fail) {
		var setConfig = true;
		db.run("CREATE TABLE if not exists config (id INTEGER PRIMARY KEY AUTOINCREMENT, port INTEGER)");
		db.run("CREATE TABLE if not exists app (id INTEGER PRIMARY KEY AUTOINCREMENT, name String, port INTEGER)");
		db.run("CREATE TABLE if not exists collection (id INTEGER PRIMARY KEY AUTOINCREMENT, name String, description String, app_id INTEGER)"); 
		db.run("CREATE TABLE if not exists test (id INTEGER PRIMARY KEY AUTOINCREMENT, name String, url String, method String, collection_id INTEGER)"); 
		db.run("CREATE TABLE if not exists mem (stat REAL, time REAL, app String)"); 
		db.run("CREATE TABLE if not exists cpu_load (stat REAL, time REAL, app String)"); 
		db.run("CREATE TABLE if not exists req (url String, time REAL, app String)");
		done();
	});
};

var configSet = function() {
	return new Promise(function(done,fail) { 
		var setConfig = true;
		db.get("SELECT * FROM config", function(err,row) {
			if(row == undefined) {
				setConfig = false;
			}
			done(setConfig);
		});
	});
};

var saveConfig = function(port) {
	configSet()
	.then(function(set) {
		if(set) {
			//db.run()
		} else {
			db.run("INSERT INTO config (port) VALUES ($port)", {
	  		$port: port
	 		}); 
		}
	});	
};

var saveApp = function(name,port) {
	return new Promise(function(done,fail) {
		db.run("INSERT INTO app (name, port) VALUES ($name, $port)", {
		  $name: name,
		  $port: port
		}, function(err) {
			if(err) {
				fail(err);
			} else {
				done();
			}
		})
	}); 
};

var saveReq = function(name,url) {
	return new Promise(function(done,fail) {
		var date = new Date();
		//var time = date.toString();
		var time = date.getTime();
		db.run("INSERT INTO req (app,url,time) VALUES ($name, $url, $time)" , {
			$name : name,
			$url : url,
			$time : time
		}, function(err) {
			if(err) {
				fail(err);
			} else {
				done();
			}
		})
	});
}

var getReq = function(name) {
	return new Promise(function(done,fail) {
		db.all("SELECT url,time FROM req WHERE app=$app ORDER BY time DESC LIMIT 200",{ $app : name },function(err,rows) {
			if(err) {
				fail(err);
			} else {
				done(rows);
			}
		});
	});
}

var getReqRange = function(app,from,to) {
	return new Promise(function(done,fail) {
		db.all("SELECT url,time FROM req WHERE (app=$app) AND (time >= $from) AND (time <= $to)",{ $app : app, $from : from, $to : to},function(err,rows) {
			if(err) {
				fail(err);
			} else {
				done(rows);
			}
		});
	});
}

var saveCPULoad = function(name,load) {
	return new Promise(function(done,fail) {
		var date = new Date();
		//var time = date.toString();
		var time = date.getTime();
		db.run("INSERT INTO cpu_load (app,stat,time) VALUES ($name, $load, $time)" , {
			$name : name,
			$load : load,
			$time : time
		}, function(err) {
			if(err) {
				fail(err);
			} else {
				done();
			}
		})
	});
};

var getCPULoad = function(app) {
	return new Promise(function(done,fail) {
		db.all("SELECT stat,time FROM cpu_load WHERE app=$app ORDER BY time DESC LIMIT 10",{ $app : app },function(err,rows) {
			if(err) {
				fail(err);
			} else {
				done(rows);
			}
		});
	});
};

var getCPULoadRange = function(app,from,to) {
	return new Promise(function(done,fail) {
		db.all("SELECT stat,time FROM cpu_load WHERE (app=$app) AND (time >= $from) AND (time <= $to)",{ $app : app, $from : from, $to : to},function(err,rows) {
			if(err) {
				fail(err);
			} else {
				done(rows);
			}
		});
	});
};

var saveMem = function(name,stat) {
	return new Promise(function(done,fail) {
		var date = new Date();
		var time = date.getTime();
		db.run("INSERT INTO mem (app,stat,time) VALUES ($name, $stat, $time)" , {
			$name : name,
			$stat : stat,
			$time : time
		}, function(err) {
			if(err) {
				fail(err);
			} else {
				done();
			}
		})
	});
};

var getMem = function(app) {
	return new Promise(function(done,fail) {
		db.all("SELECT stat,time FROM mem WHERE app=$app ORDER BY time DESC LIMIT 10",{ $app : app },function(err,rows) {
			if(err) {
				fail(err);
			} else {
				done(rows);
			}
		});
	});
};

var getMemRange = function(app,from,to) {
	return new Promise(function(done,fail) {
		db.all("SELECT stat,time FROM mem WHERE (app=$app) AND (time >= $from) AND (time <= $to)",{ $app : app, $from : from, $to : to},function(err,rows) {
			if(err) {
				fail(err);
			} else {
				done(rows);
			}
		});
	});
};

var getApps = function() {
	return new Promise(function(done,fail) {
		db.all("SELECT * FROM app",{},function(err,rows) {
			if(err) {
				fail(err);
			} else {
				done(rows);
			}
		});
	});
};

var getAppId = function(name) {
	return new Promise(function(done,fail) {
		db.get("SELECT * FROM app WHERE name=$name",{ $name : name } , function(err,row) {
			if(err) { fail(err) }
				else {
					done(row);
				}
		});
	});
}

var getCollections = function(name) {
	return new Promise(function(done,fail) {
		getAppId(name)
		.then(function(app) {
			db.all("SELECT * FROM collection WHERE app_id=$id",{ $id : app.id },function(err,rows) {
				if(err) {
					fail(err);
				} else {
					done(rows);
				}
			});
		});
	});
}


var getCollection = function(name) {
	return new Promise(function(done,fail) {
		db.get("SELECT * FROM collection WHERE name=$name",{ $name : name } , function(err,row) {
			if(err) { fail(err) }
				else {
					done(row);
				}
		});
	});
}

var saveTestsCollection = function(appName,name,description) {
	return new Promise(function(done,fail) {
		getAppId(appName)
		.then(function(app) {
			db.run("INSERT INTO collection (name, description, app_id) VALUES ($name, $desc, $id)", {
			  $name: name,
			  $desc: description,
			  $id : app.id

			}, function(err) {
				if(err) {
					fail(err);
				} else {
					done();
				}
			});
		})
		.catch(function(err) {
			fail(err);
		});
	});
}

var saveTest = function(collectionName,name,url,method) {
	return new Promise(function(done,fail) {
		getCollection(collectionName)
		.then(function(collection) {
			db.run("INSERT INTO test (name, url, method, collection_id) VALUES ($name, $url, $method, $id)", {
				$name : name,
				$url : url,
				$method : method,
				$id : collection.id
			}, function(err) {
				if(err) {
					fail(err);
				} else {
					done();
				}
			});
		})
		.catch(function(err) {
			fail(err);
		});
	});
}

var getTests = function(name) {
	return new Promise(function(done,fail) {
		getCollection(name)
		.then(function(collection) {
			db.all("SELECT * FROM test WHERE collection_id=$id",{ $id : collection.id },function(err,rows) {
				if(err) {
					fail(err);
				} else {
					done(rows);
				}
			});
		});
	});
}



module.exports = {
  init: init,
  configSet: configSet,
  saveConfig : saveConfig,
  saveApp : saveApp,
  getApps : getApps,
  getAppId : getAppId,
  saveTestsCollection : saveTestsCollection,
  getCollections : getCollections,
  getCollection : getCollection,
  saveTest : saveTest,
  getTests : getTests,
  saveCPULoad : saveCPULoad,
  saveMem : saveMem,
  getCPULoad : getCPULoad,
  getCPULoadRange : getCPULoadRange,
  getMem : getMem,
  getMemRange : getMemRange,
  saveReq : saveReq,
  getReq : getReq,
  getReqRange : getReqRange
}


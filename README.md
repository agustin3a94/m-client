# M Client

### Installation

```sh
$ sudo npm install
$ sudo npm install -g nw
```
Build sqlite3 for node-webkit

--target-arch -> x64 | x32

--target -> Default 0.12.3(Change if is a different version of nw).

```sh
$ sudo npm install sqlite3 --build-from-source --runtime=node-webkit --target_arch=x64 -target=0.12.3
```

### Run

```sh
$ sudo nw
```




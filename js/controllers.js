angular.module('app.controllers', [])
   .controller("appCtrl", function(){
        //código del controlador (lo estoy usando en todas las rutas, en este sencillo ejemplo)
    })

    .controller("homeCtrl", ['$scope','$location', function($scope,$location) {
      //$scope.init = false;

      $scope.alert = true;
      if(global.alert != null) {
        $scope.alert = false;
        $scope.alert_message = global.alert;
        global.alert = null;
      }
      $scope.no_apps = false;
      $scope.init = true; 
      db.init()
      .then(function() {
        return db.configSet();
      })
      .then(function(sc) {
    		console.log(sc);
    		if(!sc) {
    			$location.path('/step1');  
    		}
          $scope.init = false;
          return db.getApps();
    	})
      .then(function(apps) {
        $scope.apps = apps;
        console.log(apps);
        if($scope.apps.length > 0) {
          $scope.no_apps = true;
        }
        $scope.$apply();
        //console.log($scope.apps);
      });

    }])

    .controller("welcomeCtrl", ['$scope','$location','$timeout', function($scope,$location,$timeout) {
        
        // STEP1
        $scope.step1 = function() {
          //validate port
           //console.log("Port " + $scope.port)
          if(isNaN($scope.port) || ($scope.port == "") || ($scope.port < 0)) {
            $scope.errorP = "Invalid port";
            $scope.invalidP = true;
          } else if($scope.port <= 1024) {
            $scope.errorP = "Forbidden Port";
            $scope.invalidP = true;
          } else {
            $scope.invalidP = false;  
            $scope.next = true;
           utils.checkPort($scope.port)
           .then(function(status) {
              
              if(status) {
                db.saveConfig($scope.port);
                $location.path('/step2');
                monitorServer.start($scope.port);
              } else {
                $scope.errorP = "Port currently in use";
                $scope.invalidP = true;
              }
              $scope.next = false;
              $scope.$apply();
           })
           .catch(function(err) {
            console.log(err);
           });
          }
        };


        // STEP2
        $scope.finish = function() {
            $location.path('/');
        };

    }]);
var PORT, HOST;
var dgram  = require('dgram');
		


var start = function(port) {
	var server = dgram.createSocket('udp4');
	PORT = port; 
	HOST = '127.0.0.1';
	server.on('listening', function () {
	  var address = server.address();
	  console.log('UDP Server listening on ' + address.address + ":" + address.port);
	});

	server.on('message', function (message, remote) {
	 console.log(remote.address + ':' + remote.port +' - ' + message);
	 	var status = JSON.parse(message);
	 	var appName = status.app;
	 	var type = status.type;
	 	var load,mem, url;
	 	if(global.test) {
	 		
	 	} else {
		 	if(type == 'cpu-status') {
		 		load = status.load;
		 		mem = status.mem;
		 		global.stat.load = load;
		 		global.stat.mem = mem;
		 		global.db.saveCPULoad(appName,load)
		 		.then(function() {
		 			return global.db.saveMem(appName,mem);
		 		}).catch(function(err) {
		 			console.log(err);
		 		});
		 	} else if(type == 'req') {
		 		url = status.url;
		 		global.db.saveReq(appName,url)
		 		.then(function(){

		 		}).catch(function(err) {
		 			console.log(err);
		 		});
		 	}	
		}
	});

	server.bind(PORT, HOST);
}

module.exports = {
  start : start
};


var portscanner = require('portscanner');
var Promise = require('bluebird');

exports.checkPort = function(port) {
		return new Promise(function(done,fail) {
			portscanner.checkPortStatus(port, '127.0.0.1', function(error, status) {
		  	console.log(port + " " + status);
		  	var r = false;
		  	if(status == 'closed') {
		  		r = true;
		  	}
		  	done(r);
			});
		});
};
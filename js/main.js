// DB CONNECTION
var db = require('./db/dbconnection.js');
var utils = require('./js/monitor/utils.js');
var monitorServer = require('./js/monitor/server.js');
var _ = require('lodash');
var array = require('lodash/array');

global.db = db;

global.stat = {};

monitorServer.start(4000);

global.alert = null;

//var ui = require('./js/ui.js');
db.init()
.then(function() {
    console.log('DB init');
});


/*
db.saveApp("example",1337)
.then(console.log("SAVE"))
.catch(function(err) {
    console.log(err);
});
*/





angular.module("app", ["app.controllers","app.AppControllers","app.TestControllers","app.StatsControllers","app.EventsControllers","ngRoute","ladda","highcharts-ng"])
    .config(function($routeProvider){
        $routeProvider
            .when("/", {
                controller: "homeCtrl",
                controllerAs: "vm",
                templateUrl: "views/home.html"
            })
            .when("/config", {
                controller: "appCtrl",
                controllerAs: "vm",
                templateUrl: "views/config.html"
            })
            .when("/step1", {
                controller: "welcomeCtrl",
                templateUrl: "views/init/step1.html"
            })
            .when("/step2", {
                controller: "welcomeCtrl",
                templateUrl: "views/init/step2.html"
            })
            .when("/opciones", {
                controller: "appCtrl",
                controllerAs: "vm",
                templateUrl: "opciones.html"
            })
            .when("/app/new", {
                controller: "newAppCtrl",
                controllerAs: "vm",
                templateUrl: "views/app/new.html"
            })
            .when("/app/:name/dashboard", {
                controller: "dashboardAppCtrl",
                controllerAs : "vm",
                templateUrl: "views/app/dashboard.html"
            })
            .when("/app/:name/config", {
                controller: "configAppCtrl",
                controllerAs : "vm",
                templateUrl: "views/app/config.html"
            })
            .when("/app/:name/tests", {
                controller: "indexTestCtrl",
                controllerAs : "vm",
                templateUrl: "views/test/index.html"
            })
            .when("/app/:name/tests/collection/new", {
                controller: "collectionNewCtrl",
                controllerAs : "vm",
                templateUrl: "views/test/new_collection.html"
            })
            .when("/app/:name/tests/collection/:collection", {
                controller: "collectionCtrl",
                controllerAs : "vm",
                templateUrl: "views/test/collection.html"
            })
            .when("/app/:name/tests/collection/:collection/new", {
                 controller: "testNewCtrl",
                controllerAs : "vm",
                templateUrl: "views/test/new_test.html"
            })
            .when("/app/:name/tests/collection/:collection/run", {
                controller : "runTestCtrl",
                templateUrl : "views/test/run.html"
            })
            .when("/app/:name/tests/collection/:collection/load_test", {
                controller : "loadTestCtrl",
                templateUrl : "views/test/load_test.html"
            })
            .when("/app/:name/stats/mem", {
                controller : "memStatCtrl",
                templateUrl : "views/stats/mem.html"
            })
            .when("/app/:name/stats/web", {
                controller : "webStatCtrl",
                templateUrl : "views/stats/web.html"
            })
            .when("/app/:name/stats", {
                controller : "indexStatCtrl",
                templateUrl : "views/stats/index.html"
            })
            .when("/app/:name/events", {
                controller : "indexEventCtrl",
                templateUrl : "views/events/index.html"
            });



    });
    
angular.module('app.AppControllers', [])
   .controller("newAppCtrl", [ '$scope', '$location', function($scope,$location){

      $scope.app = {};

      $scope.newApp = function() {
        console.log($scope.app.name);
        console.log($scope.app.port);
        
        db.saveApp($scope.app.name,$scope.app.port)
        .then(function() {
          global.alert = "App " + $scope.app.name + " added";
          $location.path('/');
          $scope.$apply();
        })
        .catch(function(err) {
          console.log(err);
        });
        
      };

        /*
        db.saveApp("example",1337)
        .then(console.log("SAVE"))
        .catch(function(err) {
          console.log(err);
        });
        */ 
    }])

   .controller("dashboardAppCtrl", ['$scope','$routeParams', function($scope,$routeParams) {
   	$scope.name = $routeParams.name;



 $scope.highchartsNG = {
        options: {
            chart: {
                type: 'spline'
            }
        },
        series: [{
            name : 'Requests',
            color: 'rgb(61,157,113)',
            data: [10, 15, 12, 8, 7]
        }],
        title: {
            text: 'Requests'
        },

        xAxis : {
            type: 'datetime'
        },
        loading: false
    }

    $scope.CPU = {
        options: {
            chart: {
                type: 'solidgauge',
               backgroundColor: '#fff'
              
            },
            pane: {
                center: ['50%', '85%'],
                size: '100%',
                startAngle: -90,
                endAngle: 90,
                background: {
                    backgroundColor:'#f9f9fb',
                    innerRadius: '60%',
                    outerRadius: '100%',
                    shape: 'arc',
                  borderColor: 'transparent'
                }
            },
            gauge: {
                dataLabels: {
                    y: -30,
                    borderWidth: 0,
                    useHTML: true
                }
            }
        },
        series: [{
            data: [0],
            
            dataLabels: {
                borderWidth: 0,
                format: '<div style="text-align:center"><span style="font-size:25px;color:black";border-width:0>{y}%</span>' + 
                    '</div>'
            }
            
        }],
        title: {
            text: 'CPU Load',
            y: 50
        },
        yAxis: {
            currentMin: 0,
            currentMax: 100,
            stops: [
                  [0, '#4fc5cd'],
                  [1, '#73b767']
                ],
            from: 0,
            to: 100,         
                        lineWidth: 0,
           // tickInterval:0,
            tickPixelInterval: 400,
            labels: {
                y: 15
            }   
        },
   
        loading: false
    };  

    $scope.Mem = {
        options: {
            chart: {
                type: 'solidgauge',
               backgroundColor: '#fff'
              
            },
            pane: {
                center: ['50%', '85%'],
                size: '100%',
                startAngle: -90,
                endAngle: 90,
                background: {
                    backgroundColor:'#f9f9fb',
                    innerRadius: '60%',
                    outerRadius: '100%',
                    shape: 'arc',
                  borderColor: 'transparent'
                }
            },
            gauge: {
                dataLabels: {
                    y: -30,
                    borderWidth: 0,
                    useHTML: true
                }
            }
        },
        series: [{
            data: [75],
            
            dataLabels: {
                borderWidth: 0,
                format: '<div style="text-align:center"><span style="font-size:25px;color:black";border-width:0>{y}%</span>' + 
                    '</div>'
            }
            
        }],
        title: {
            text: 'Process Memory Usage',
            y: 50
        },
        yAxis: {
            currentMin: 0,
            currentMax: 100,
            stops: [
                  [0, '#4fc5cd'],
                  [1, '#73b767']
                ],
            from: 0,
            to: 100,         
                        lineWidth: 0,
           // tickInterval:0,
            tickPixelInterval: 400,
            labels: {
                y: 15
            }   
        },
   
        loading: false
    }; 
    
var x;

if(global.stat.load != undefined) {
            //var point = $scope.CPU.series[0].points[0];
            x = parseInt(global.stat.load);
            $scope.CPU.series[0].data[0] = x;
        }
    
     setInterval(function() {
      var r = Math.floor((Math.random() * 10) + 1);
      $scope.highchartsNG.series[0].data.push(r);

        if(global.stat.load != undefined) {
            //var point = $scope.CPU.series[0].points[0];
            x = parseInt(global.stat.load);
            $scope.CPU.series[0].data[0] = x;
           $scope.$apply();
        }

        $scope.$apply();
      },3000);
   

   	$scope.addPoints = function () {
        var seriesArray = $scope.highchartsNG.series
        var rndIdx = Math.floor(Math.random() * seriesArray.length);
        seriesArray[rndIdx].data = seriesArray[rndIdx].data.concat([1, 10, 20])
    };

    $scope.addSeries = function () {
        var rnd = []
        for (var i = 0; i < 10; i++) {
            rnd.push(Math.floor(Math.random() * 20) + 1)
        }
        $scope.highchartsNG.series.push({
            data: rnd
        })
    }

    $scope.removeRandomSeries = function () {
        var seriesArray = $scope.highchartsNG.series
        var rndIdx = Math.floor(Math.random() * seriesArray.length);
        seriesArray.splice(rndIdx, 1)
    }

    $scope.options = {
        type: 'line'
    }

    $scope.swapChartType = function () {
        if (this.highchartsNG.options.chart.type === 'line') {
            this.highchartsNG.options.chart.type = 'bar'
        } else {
            this.highchartsNG.options.chart.type = 'line'
        }
    }

    
   }])

   .controller("configAppCtrl", ['$scope','$routeParams', function($scope,$routeParams) {
   	$scope.name = $routeParams.name;
    $scope.app = {};
    $scope.app.name = $scope.name
   	//console.log($scope.name);
   }])


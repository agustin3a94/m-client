var loadtest = require('loadtest'); 
var maxRequests = 0,
      concurrency = 0,
      maxTime     = 0,
      load_tests;

angular.module('app.TestControllers', [])


   .controller("indexTestCtrl", ['$scope','$routeParams', function($scope,$routeParams) {
   	$scope.name = $routeParams.name;

      $scope.alert = true;
      if(global.alert != null) {
        $scope.alert = false;
        $scope.alert_message = global.alert;
        global.alert = null;
      }

   	db.getCollections($scope.name)
      .then(function(collections) {
         console.log(collections);
         $scope.collections = collections;
          $scope.$apply();
      })
      .catch(function(err) {
         console.log(err);
      });
   }])

    .controller("collectionNewCtrl", ['$scope','$routeParams', '$location', function($scope,$routeParams,$location) {
      $scope.name = $routeParams.name;
      $scope.create = function() {
         
         db.saveTestsCollection($scope.name,$scope.nameC, $scope.description)
         .then(function() {
            global.alert = "Collection " + $scope.nameC + " added";
            $location.path('/app/' + $scope.name + '/tests');
            $scope.$apply();
         })
         .catch(function(err) {
            console.log(err);
         });
         
         //console.log($scope.nameC + " " + $scope.description);
      };
   }])

   .controller("collectionCtrl", ['$scope','$routeParams', function($scope, $routeParams) {
      $scope.name = $routeParams.name;
      $scope.collectionName = $routeParams.collection;
      db.getCollection($scope.collectionName)
      .then(function(collection) {
         $scope.collection = collection;
         $scope.$apply();
         return db.getTests(collection.name);
      })
      .then(function(tests) {
         $scope.tests = tests;
         $scope.$apply();
      })
      .catch(function(err) {
         console.log(err);
      });
   }])

     .controller("testNewCtrl", ['$scope','$routeParams','$location', function($scope, $routeParams, $location) {
      $scope.name = $routeParams.name;
      $scope.collectionName = $routeParams.collection;
      $scope.init = true;
      $scope.apiForm = true;
      $scope.scanForm = false;
      $scope.fields = {};
      $scope.continue = function(op) {
         $scope.init = false;
         if(op == 0) {
            $scope.apiForm = true;
         } else {
            $scope.scanForm = true;
         }
      };


     $scope.scan = function() {
         var per = 0,
            timerId = 0,
            aux;
         $scope.pStyle = { 'width': '0%' };
         timerId = setInterval(function() {
            per = per + 20;
            aux = per + '%';
            $scope.pStyle = { 'width' : aux };
            $scope.$apply();
            if(per >= 120) {

               clearInterval(timerId);
               $('#myModal').modal('hide');
               $scope.scanForm = false;
               $scope.apiForm = true;
               $scope.params[0] = {key : "username"};
               $scope.params[1] = {key : "comment"};
               $scope.$apply();
            }
         },500);
     }

      $scope.return = function() {
         $scope.init = true;
         $scope.apiForm = false;
         $scope.scanForm = false;
      };
      
      $scope.params = [{key: ""}];
      $scope.addNewParam = function() {
         $scope.params.push({ key : ""});
      };
      $scope.removeParam = function() {
         var l = $scope.params.length - 1;
         $scope.params.splice(l);
      };
      $scope.createTest = function() {
         console.log($scope.fields.name);
         console.log($scope.fields.method);
         console.log($scope.fields.url);
         console.log($scope.params);
         var url = makeUrl($scope.fields.url,$scope.params);
         console.log(url);

         
         db.saveTest($scope.collectionName,$scope.fields.name,url,$scope.fields.method)
         .then(function() {
            $location.path('/app/' + $scope.name + '/tests/collection/' + $scope.collectionName);
            $scope.$apply();
         })
         .catch(function(err) {
            console.log(err);
         });

      };  
   }])



   .controller("runTestCtrl", ['$scope','$routeParams','$location', function($scope, $routeParams, $location) {
      $scope.name = $routeParams.name;
      $scope.collectionName = $routeParams.collection;
      $scope.alert = {};
      $scope.alert.hide = true;
      $scope.run = {};
      var ready = false;
      db.getCollection($scope.collectionName)
      .then(function(collection) {
         $scope.collection = collection;
         $scope.$apply();
         return db.getTests(collection.name);
      }).then(function(tests) {
         $scope.tests = tests;
         $scope.$apply();
      }).catch(function(err) {
         console.log(err);
      });

      $scope.run = function() {
        ready = false;
        $scope.alert.hide = true;
        
        console.log("Max req " + $scope.run.maxRequests + " concurrency " + $scope.run.concurrency + " max time " + $scope.run.maxTime);
        

        load_tests = $scope.getTests($scope.tests);
        
        

        if(($scope.run.maxTime == undefined) || ($scope.run.maxTime == "")) {
            $scope.alert.hide = false;
            $scope.alert.message = "Max Time required";
        } else {
          if(isNaN($scope.run.maxTime)) {
            $scope.alert.hide = false;
            $scope.alert.message = "Max Time is not a number";
          } else {
            $scope.alert.hide = true;
          }
          
        }
        if(load_tests.length == 0) {
          $scope.alert.hide = false;
          $scope.alert.message = "Select at least one test";
        }
        if(($scope.run.concurrency == undefined) || ($scope.run.maxRequests == "")) {
          $scope.alert.hide = false;
          $scope.alert.message = "Concurrency required";
        } else {
          if(isNaN($scope.run.concurrency)) {
            $scope.alert.hide = false;
            $scope.alert.message = "Concurrency is not a number";
          }
        }
        if(($scope.run.maxRequests == undefined) || ($scope.run.maxRequests == "")) {
          $scope.alert.hide = false;
          $scope.alert.message = "Max Requests required";
        } else {
           if(isNaN($scope.run.maxRequests)) {
            $scope.alert.hide = false;
            $scope.alert.message = "Max Requests is not a number";
          } else {
            ready = true;
          }
        }

        

        if($scope.alert.hide) {
          console.log("Ready");
          maxRequests = $scope.run.maxRequests ;
          concurrency = $scope.run.concurrency;
          maxTime     = $scope.run.maxTime;
          $location.path("/app/" + $scope.name + "/tests/collection/" + $scope.collectionName + "/load_test");
        }
        
         


        /*
        } else if(isNaN($scope.run.maxTime)) {
          console.log("Max time not a number");
        }
        */
         //$location.path("/app/" + $scope.name + "/tests/collection/" + $scope.collectionName + "/load_test");
         //$scope.$apply();
      };

      $scope.getTests = function(tests) {
        var test,
            r = new Array();
        for(var i = 0; i < tests.length; i++) {
          test = tests[i];
          if((test.check != undefined) && (test.check == true)) {
            r.push(test);
          }
        }
        return r;
      }

   }])

.controller("loadTestCtrl", ['$scope','$routeParams','$location', function($scope, $routeParams, $location) {

    $scope.result = {};

      Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });

      $scope.name = $routeParams.name;
      $scope.collectionName = $routeParams.collection;

    $scope.completed = false;
    $scope.passed = false;
    $scope.fail = false;
    var current = new Date();

   $scope.Latency = {
     options: {
         chart: {
             type: 'spline'
         }
     },
     minorGridLineWidth: 0,
            gridLineWidth: 0,
            alternateGridColor: null,
      plotOptions: {
spline: {
                lineWidth: 4,
                states: {
                    hover: {
                        lineWidth: 5
                    }
                },
                marker: {
                    enabled: false
                },
      
            }
    },
     series: [{
         name : 'Latency',
         data: [],
         marker: {
                    enabled: false
                }

     }],
     title: {
         text: 'Latency'
     },
     yAxis: {
            title: {
                text: 'Latency (ms)'
            }
        },

     xAxis : {
         type: 'datetime'
     },
     loading: false
   }

 $scope.CPU = {
   options: {
       chart: {
           type: 'spline'
       }
   },
   series: [{
       name : 'CPU Load',
       data: [5,5,5],
       marker: {
                    enabled: false
                }
   }],
   title: {
       text: 'CPU Load'
   },

   xAxis : {
       type: 'datetime'
   },
   yAxis : {

            title: {
                text: 'Load (%)'
            },
    min : 0,
    max : 100,
    plotBands: [{ // Light air
                from: 90,
                to: 100,
                color: 'rgba(255, 0, 0,0.1)'
            }, { // Light breeze
                from: 30,
                to: 90,
                color: 'rgba(127, 255, 0,0.1)'
                
            }, { // Gentle breeze
                from: 0,
                to: 30,
                color: 'rgba(0, 255, 0,0.1)'
                
            }
            ]
   },
   loading: false
 }

  $scope.Mem = {
   options: {
       chart: {
           type: 'spline'
       }
   },
   series: [{
       name : 'Memory Usage',
       data: [],
       marker: {
                    enabled: false
                }
   }],
   title: {
       text: 'Memory Usage'
   },

   xAxis : {
       type: 'datetime'
   },
   yAxis: {
            title: {
                text: 'Memory (bytes)'
            }
        },
   loading: false
 }

 var totalMaxLatency = [],
     totalMinLatency = [],
     totalErrors = 0,
     totalTime = 0,
     totalRequests = 0;


var x;
var per = 0;
var index = 0;
function runLoadTest(tests) {
  function statusCallback(latency, result) {
      var current_latency = (latency.maxLatencyMs + latency.meanLatencyMs)/2;
      $scope.Latency.series[0].data.push(current_latency);
      if(global.stat.load != undefined) {
        x = parseInt(global.stat.load);
        $scope.CPU.series[0].data.push(x);
        $scope.$apply();
      }
      if(global.stat.mem != undefined) {
        x = parseInt(global.stat.mem);
        $scope.Mem.series[0].data.push(x);
        $scope.$apply();
      }
      per = per + 100/30;
      aux = per + '%';
      $scope.pStyle = { 'width' : aux };
      $scope.$apply();
  }

  var options = {
      url: tests[index].url,
      maxRequests: maxRequests,
      maxSeconds: maxTime,
      concurrency : concurrency,
      statusCallback: statusCallback
  };
   
  loadtest.loadTest(options, function(error,result) {
      if (error) {
          return console.error('Got an error: %s', error);
      }
      console.log('Tests run successfully');
      index++;
      totalTime = totalTime + result.totalTimeSeconds;
      totalRequests = totalRequests + result.totalRequests;
      totalErrors = totalErrors + result.totalErrors;
      totalMaxLatency.push(result.maxLatencyMs);
      totalMinLatency.push(result.meanLatencyMs);
      if(index == tests.length) {
        console.log("Sali"); 
        var maxL = 0,
            minL = 0;
        for(var i = 0; i < totalMaxLatency.length; i++) {
          maxL = maxL + totalMaxLatency[i];
          minL = minL + totalMinLatency[i];
        }
        maxL = maxL / totalMaxLatency.length;
        minL = minL / totalMinLatency.length;
        $scope.result.maxLatency = maxL;
        $scope.result.minLatency = minL;
        $scope.result.errors = totalErrors;
        if(result.totalErrors > 0) {
          $scope.fail = true;
        } else {
          $scope.passed = true;
        }
        $scope.result.time = totalTime;
        $scope.result.totalRequests = totalRequests;
        $scope.completed = true;
        $scope.pStyle = { 'width' : 1000 };
        $scope.$apply();
      } else {
        runLoadTest(tests);
      }
  });
}

runLoadTest(load_tests);

   


   }])


var makeUrl = function(url,params) {
   var r = url,
       param,
       init = true;
   for(var i = 0; i < params.length; i++) {
      param = params[i];
      if(!(param.key == "")) {
         if(!(param.value == "")) {
            if(init == true) {
               r = r + "?" + param.key + "=" + param.value;
               init = false;
            } else {
               r = r + "&" + param.key + "=" + param.value;
            } 
         }
      }
   }

   return r;
}




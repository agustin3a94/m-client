var prettyBytes = require("pretty-bytes");

angular.module('app.StatsControllers', ["highcharts-ng"])

	.controller("indexStatCtrl", [ '$scope', '$location', '$routeParams' , function($scope,$location,$routeParams){
		Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });

		$scope.name = $routeParams.name;
		$scope.CPU = {
        options: {
            chart: {
                 type: 'spline'
            }
        },
        xAxis: {
                type: 'datetime'
            },
        yAxis: {
            title: {
                text: 'Load (%)'
            }
        },
        plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null
                }
            },

        series: [{
        	name : 'Load',
        	type : 'area',
        	color: 'rgb(61,157,113)',
           data: []
        }],
        title: {
            text: 'CPU'
        },
        loading: false
    }
    $scope.cpu = {};
    $scope.cpu.to = new Date();
    $scope.cpu.from = new Date();

    db.getCPULoad($scope.name)
     .then(function(rows) {
     	console.log(rows);
     		var stats = _.map(rows, function(n) {
     			var arr = [];
     			arr.push(n.time);
     			arr.push(n.stat);
     			return arr;
     		});
     		$scope.cpu.max = _.max(stats,function(n) {
  				return n[1];
				})[1];

				$scope.cpu.min = _.min(stats,function(n) {
					return n[1];
				})[1];

				$scope.cpu.average = _.sum(stats, function(n) {
  				return n[1];
  			}) / stats.length;
     		//console.log('MAX->' + max + " MIN->" + min);
     		stats = stats.reverse();
     		$scope.CPU.series[0].data = stats;
     		$scope.$apply();
     		console.log(stats);
     }).catch(function(err) {
     	console.log(err);
     });

    $scope.filterCPU = function() {
    	var from = $scope.cpu.from.getTime(),
    			to = $scope.cpu.to.getTime();
    	if(to <= from) {
    		console.log("TO DO VALIDATION");
    	} else {
    		db.getCPULoadRange($scope.name,from,to)
     .then(function(rows) {
     	console.log(rows);
     		var stats = _.map(rows, function(n) {
     			var arr = [];
     			arr.push(n.time);
     			arr.push(n.stat);
     			return arr;
     		});
     		$scope.cpu.max = _.max(stats,function(n) {
  				return n[1];
				})[1];

				$scope.cpu.min = _.min(stats,function(n) {
					return n[1];
				})[1];

				$scope.cpu.average = _.sum(stats, function(n) {
  				return n[1];
  			}) / stats.length;
     		//console.log('MAX->' + max + " MIN->" + min);
     		stats = stats.reverse();
     		$scope.CPU.series[0].data = stats;
     		$scope.$apply();
     		console.log(stats);
     }).catch(function(err) {
     	console.log(err);
     });
    	}
    	console.log("From " + from + " To " + to);
    }
  
     
  }])


.controller("memStatCtrl", [ '$scope', '$location', '$routeParams' , function($scope,$location,$routeParams){
		$scope.name = $routeParams.name;
		Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });
		$scope.Mem = {
        options: {
            chart: {
                 type: 'area'
            }
        },
        xAxis: {
                type: 'datetime'
            },
        yAxis: {
            title: {
                text: 'Memory (bytes)'
            }
        },
        plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null
                }
            },

        series: [{
        	name : 'Memory',
        	type : 'area',
        	color: 'rgb(61,157,113)',
           data: []
        }],
        title: {
            text: 'Process Memory Usage'
        },
        loading: false
    }
		
   
    $scope.mem = {};
    $scope.mem.to = new Date();
    $scope.mem.from = new Date();

    db.getMem($scope.name)
     .then(function(rows) {
     	console.log(rows);
     		var stats = _.map(rows, function(n) {
     			var arr = [];
     			arr.push(n.time);
     			arr.push(n.stat);
     			return arr;
     		});
     		$scope.mem.max = _.max(stats,function(n) {
  				return n[1];
				})[1];

            $scope.mem.max = prettyBytes($scope.mem.max);

				$scope.mem.min = _.min(stats,function(n) {
					return n[1];
				})[1];

             $scope.mem.min = prettyBytes($scope.mem.min);

				$scope.mem.average = _.sum(stats, function(n) {
  				return n[1];
  			}) / stats.length;

                $scope.mem.average = prettyBytes($scope.mem.average);
     		//console.log('MAX->' + max + " MIN->" + min);
     		stats = stats.reverse();
     		$scope.Mem.series[0].data = stats;
     		$scope.$apply();
     		console.log(stats);
     }).catch(function(err) {
     	console.log(err);
     });

    $scope.filterMem = function() {
    	var from = $scope.mem.from.getTime(),
    			to = $scope.mem.to.getTime();
    	if(to <= from) {
    		console.log("TO DO VALIDATION");
    	} else {
    		db.getMemRange($scope.name,from,to)
     .then(function(rows) {
     	console.log(rows);
     		var stats = _.map(rows, function(n) {
     			var arr = [];
     			arr.push(n.time);
     			arr.push(n.stat);
     			return arr;
     		});
     		$scope.mem.max = _.max(stats,function(n) {
  				return n[1];
				})[1];
            $scope.mem.max = prettyBytes($scope.mem.max);

				$scope.mem.min = _.min(stats,function(n) {
					return n[1];
				})[1];

             $scope.mem.min = prettyBytes($scope.mem.min);

				$scope.mem.average = _.sum(stats, function(n) {
  				return n[1];
  			}) / stats.length;

                $scope.mem.average = prettyBytes($scope.mem.average);
     		//console.log('MAX->' + max + " MIN->" + min);
     		stats = stats.reverse();
     		$scope.Mem.series[0].data = stats;
     		$scope.$apply();
     		console.log(stats);
     }).catch(function(err) {
     	console.log(err);
     });
    	}
    	console.log("From " + from + " To " + to);
    }
  
     
  }])

.controller("webStatCtrl", [ '$scope', '$location', '$routeParams' , function($scope,$location,$routeParams){
    $scope.name = $routeParams.name;
    $scope.web = {};
    $scope.web.to = new Date();
    $scope.web.from = new Date();
    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });
        $scope.WebA = {
        options: {
            chart: {
                 type: 'spline'
            }
        },
        xAxis: {
                type: 'datetime'
            },
        yAxis: {
            title: {
                text: 'Requests'
            }
        },
        plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null
                }
            },

        series: [{
            name : 'Requests',
            color: 'rgb(61,157,113)',
           data: []
        }],
        title: {
            text: 'Web Activity'
        },
        loading: false
    }

    $scope.FileCounter = {
        options: {
            chart: {
                 type: 'bar'
            }
        },
        
        yAxis: {
            title: {
                text: 'Count'
            }
        },
        plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null
                }
            },

        series: [
        ],
        title: {
            text: 'Request Counter'
        },
        loading: false
    }

    $scope.FileTypeCounter = {
        options: {
            chart: {
                 type: 'column'
            }
        },
        xAxis : {
            title : {
                text : 'Type'
            }
        },
        yAxis: {
            title: {
                text: 'Count'
            }
        },
        plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null
                }
            },

        series: [
        ],
        title: {
            text: 'File Type Counter'
        },
        loading: false
    }



    db.getReq($scope.name)
     .then(function(rows) {
        console.log(rows);
            var stats = _.map(rows, function(n) {
                var arr = [];
                arr.push(n.time);
                arr.push(n.url);
                return arr;
            });

            var fileCount = _.countBy(stats, function(n) {
                return n[1];
            });

            var cont = 0;
            fileCount = _.map(_.keys(fileCount),function(n) {
                var arr = [];
                arr.push(n);
                arr.push(fileCount[n]);
                
                $scope.FileCounter.series[cont] = {};
                $scope.FileCounter.series[cont].name = n;
                $scope.FileCounter.series[cont].data = [fileCount[n]];
                cont++;
                return arr;
            });



            var re = /.*\.([a-zA-z]+)/; 
            var m;
            var typeCount = _.countBy(stats, function(n) {
                if ((m = re.exec(n[1])) !== null) {
                    return m[1];
                } else {
                    return "html";
                }
            });

            typeCount  = _.map(_.keys(typeCount),function(n) {
                var arr = [];
                arr.push(n);
                arr.push(typeCount[n]);
                
                $scope.FileTypeCounter.series[cont] = {};
                $scope.FileTypeCounter.series[cont].name = n;
                $scope.FileTypeCounter.series[cont].data = [typeCount[n]];
                cont++;
                return arr;
            });
            stats = stats.reverse();
            var interval = _.first(stats)[0];
            var limit = interval + 60000;
            var aCounter = [];
            var current = 0;
            aCounter.push([interval,0]);
            _.map(stats,function(n) {
                if((n[0] <= limit) && (n[0] >= interval)) {
                    aCounter[current][1]++;
                } else {
                    interval = n[0];
                    limit = interval + 60000;
                    aCounter.push([interval,1]);
                    current++;
                }
            });



            console.log("interval " + interval + " limit " + limit);
            console.log(aCounter);
            console.log(typeCount);
            console.log(fileCount);
          
            //console.log('MAX->' + max + " MIN->" + min);
            
            //$scope.FileCounter.series[0].data = fileCount;
            $scope.WebA.series[0].data = aCounter;
            $scope.$apply();
            console.log(stats);
     }).catch(function(err) {
        console.log(err);
     });

     $scope.filterWeb = function() {
        var from = $scope.web.from.getTime(),
                to = $scope.web.to.getTime();
        if(to <= from) {
            console.log("TO DO VALIDATION");
        } else {
            db.getReqRange($scope.name,from,to)
     .then(function(rows) {
        console.log(rows);
            
            var stats = _.map(rows, function(n) {
                var arr = [];
                arr.push(n.time);
                arr.push(n.url);
                return arr;
            });

            var fileCount = _.countBy(stats, function(n) {
                return n[1];
            });

            var cont = 0;
            fileCount = _.map(_.keys(fileCount),function(n) {
                var arr = [];
                arr.push(n);
                arr.push(fileCount[n]);
                
                $scope.FileCounter.series[cont] = {};
                $scope.FileCounter.series[cont].name = n;
                $scope.FileCounter.series[cont].data = [fileCount[n]];
                cont++;
                return arr;
            });



            var re = /.*\.([a-zA-z]+)/; 
            var m;
            var typeCount = _.countBy(stats, function(n) {
                if ((m = re.exec(n[1])) !== null) {
                    return m[1];
                } else {
                    return "html";
                }
            });

            typeCount  = _.map(_.keys(typeCount),function(n) {
                var arr = [];
                arr.push(n);
                arr.push(typeCount[n]);
                
                $scope.FileTypeCounter.series[cont] = {};
                $scope.FileTypeCounter.series[cont].name = n;
                $scope.FileTypeCounter.series[cont].data = [typeCount[n]];
                cont++;
                return arr;
            });
            stats = stats.reverse();
            var interval = _.first(stats)[0];
            var limit = interval + 60000;
            var aCounter = [];
            var current = 0;
            aCounter.push([interval,0]);
            _.map(stats,function(n) {
                if((n[0] <= limit) && (n[0] >= interval)) {
                    aCounter[current][1]++;
                } else {
                    interval = n[0];
                    limit = interval + 60000;
                    aCounter.push([interval,1]);
                    current++;
                }
            });




          
            //console.log('MAX->' + max + " MIN->" + min);
            
            //$scope.FileCounter.series[0].data = fileCount;
            $scope.WebA.series[0].data = aCounter;
            $scope.$apply();

     }).catch(function(err) {
        console.log(err);
     });
        }
        console.log("From " + from + " To " + to);
    }
}]);